import React, { Component } from 'react';
import './Footer.css';

class Footer extends Component {
  render() {
    return (
      <div className="Footer">
        <h4 className="text-center">Thank You :)</h4>
      </div>
    );
  }
}

export default Footer;