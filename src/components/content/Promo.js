import React, { Component } from 'react';

class Promo extends Component {
  render() {
    let promos = this.props.promoCode.map(p => {
      return (
        <li key={p.promoId} className="list-group-item d-flex justify-content-between bg-light">
          <div className="text-success">
            <h5 className="my-0">Promo code </h5>
            <h6>{p.promoInput}</h6>
          </div>
          <span className="text-success">
            <h4 className="text-right">-฿ {p.promoDisplay}</h4>
            <button type="button" className="close" data-dismiss="alert" aria-label="Close" onClick={this.props.removePromo.bind(null, p.promoId)}>
              <span aria-hidden="true">&times;<small>remove</small></span>
            </button>
          </span>
        </li>
      );
    });
    return (
      <div>
        {promos}
      </div>
    );
  }
}

export default Promo;