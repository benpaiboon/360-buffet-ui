import React, { Component } from 'react';

class TotalPrice extends Component {
  render() {
    return (
      <li className="list-group-item d-flex justify-content-between">
        <span><h4>Total (THB)</h4></span>
        <h4><strong>฿ <span className="badge badge-success">{this.props.totalPrice}</span></strong></h4>
      </li>
    );
  }
}

export default TotalPrice;