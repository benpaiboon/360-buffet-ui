import React, { Component } from 'react';
import Persons from './Persons';
import Buffet from './Buffet';
import Promo from './Promo';
import TotalPrice from './TotalPrice';

class Billing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      totalPersons: 0,
      buffetPrice: 459,
      promoCode: [],
      promoId: 0,
      promoDiscount: 0,
      promoDisplay: 0,
      totalPrice: 0,
    }
  }

  handlePersons = () => {
    let totalPrice = this.refs.person.value * this.state.buffetPrice;
    this.setState({
      totalPersons: this.refs.person.value,
      totalPrice: Math.floor(totalPrice *  100) / 100
    });
  }

  addPromo = (e) => {
    e.preventDefault();

    let persons = this.refs.person.value;
    let promoId = this.state.promoId;
    let promoInput = this.refs.promoInput.value;
    let promoDisplay = this.state.promoDisplay;
    let promoDiscount = this.state.promoDiscount;
    let totalPrice = this.refs.person.value * this.state.buffetPrice;

    if (this.refs.person.value === '') {
      alert('Please enter total persons before add promo code.');
    }
    else if (promoInput !== '4PAY3' && promoInput !== 'LUCKY ONE' && promoInput !== 'LUCKY TWO') {
      alert('Your Promo Code is invalid.');
    }
    else {
      if (promoInput === 'LUCKY ONE') {
        promoDiscount = 0.15;
        promoDisplay = Math.floor(totalPrice * promoDiscount * 100) / 100;
      }
      if (promoInput === '4PAY3' && (persons > 0 && persons < 4)) {
        alert('This Promo Code use for only 4 persons.');
      }
      if (promoInput === '4PAY3' && persons >= 4) {
        promoDiscount = 0.25;
        promoDisplay = Math.floor(totalPrice * promoDiscount * 100) / 100;
      }
      if (promoInput === 'LUCKY TWO' && persons !== '2') {
        alert('This Promo Code use for only 2 persons.');
      }
      if (persons === '2') {
        promoDiscount = 0.20;
        promoDisplay = Math.floor(totalPrice * promoDiscount * 100) / 100;
      }
     
      let promoObj = {
        promoId,
        promoInput,
        promoDisplay,
        promoDiscount,
      };

      let promoArr = this.state.promoCode;
      promoArr.push(promoObj);

      let promoDisplayArr = [];
      promoArr.forEach(element => {
        promoDisplayArr.push(element.promoDisplay);
      });

      let finalDiscount = Math.max.apply(null, promoDisplayArr);
      promoId += 1;
      totalPrice -= finalDiscount
      
      this.setState({
        totalPrice: Math.floor(totalPrice *  100) / 100,
        promoCode: promoArr,
        promoId: promoId,
        promoDiscount: promoDiscount,
        promoDisplay: finalDiscount
      });
    }

    this.refs.promoForm.reset();
  }

  removePromo = (index) => {
    let totalPrice = this.state.totalPrice;
    let promoArr = this.state.promoCode;

    let promo = promoArr.find(function(promo){
      return promo.promoId === index;
    });
    promoArr.splice(promo, 1);
    
    totalPrice += this.state.promoDisplay;

    this.setState({
      promoCode: promoArr,
      totalPrice: Math.floor(totalPrice *  100) / 100
    })
  }

  render() {
    return (
      <div className="Billing">
        <h1 className="text-center">Billing</h1>
        <hr />
        <div className="row justify-content-center">
          <div className="col-sm-5">
            <div className="input-group input-group-lg">
              <div className="input-group-prepend">
                <span className="input-group-text" id="inputGroup-sizing-lg">Persons</span>
              </div>
              <input
                type="text"
                className="form-control"
                aria-label="Large"
                aria-describedby="inputGroup-sizing-sm"
                ref="person"
                placeholder="Enter total persons here..."
                onChange={this.handlePersons}
              />
            </div><br />
            <ul className="list-group mb-3">
            <div className="bg-danger text-white text-center">*** You can use only maximun Promo Code ***</div>
              <Persons totalPersons={this.state.totalPersons} />
              <Buffet buffetPrice={this.state.buffetPrice} />
              <Promo promoCode={this.state.promoCode} removePromo={this.removePromo} />
              <TotalPrice totalPersons={this.state.totalPersons} totalPrice={this.state.totalPrice} promoCode={this.state.promoCode} />
            </ul>
            <form className="card p-2" ref="promoForm">
              <div className="input-group">
                <input className="form-control" type="text" disabled={!this.state.totalPrice} ref="promoInput" placeholder="Promo code" />
                <div className="input-group-append">
                  <button type="submit" disabled={!this.state.totalPrice} className="btn btn-secondary" onClick={this.addPromo}>Redeem</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Billing;