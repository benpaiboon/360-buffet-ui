import React, { Component } from 'react';

class Persons extends Component {
  render() {
    return (
      <li className="list-group-item d-flex justify-content-between">
        <span><h4>Total Persons</h4></span>
        <h4><strong>{this.props.totalPersons}</strong></h4>
      </li>
    );
  }
}

export default Persons;