import React, { Component } from 'react';

class Buffet extends Component {
  render() {
    return (
      <li className="list-group-item d-flex justify-content-between">
        <span><h4>Buffet Price</h4></span>
        <h4><strong>฿ {this.props.buffetPrice}</strong></h4>
      </li>
    );
  }
}

export default Buffet;