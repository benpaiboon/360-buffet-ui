import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

// CSS
import './App.css';

// Components
import Header from './components/header/Header';
import Billing from './components/content/Billing';
import Reservation from './components/content/Reservation';
import Footer from './components/footer/Footer';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Header />
          <div className="container">
            <div className="main-content">
              <Route exact path="/" component={Billing} />
              <Route exact path="/reservation" component={Reservation} />
            </div>
          </div>
          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
