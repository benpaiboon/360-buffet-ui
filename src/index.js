import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

// Bootstrap
import './assets/css/cosmo.min.css';
// import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/jquery/dist/jquery.min';
import '../node_modules/popper.js/dist/umd/popper';
import '../node_modules/bootstrap/dist/js/bootstrap.min';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
